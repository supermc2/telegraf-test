const Telegraf = require('telegraf');
const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite'
});

//const User = require('./user.js')
require('dotenv').config();
sequelize
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});
const User = sequelize.import('./model/user.js')
const Asset = sequelize.import('./model/asset.js')
const Loan = sequelize.import('./model/loan.js')

const bot = new Telegraf(process.env.BOT_TOKEN)
const chat_id = process.env.chat_id
userMap = new Map();
assetMap = new Map();

// Note: using `force: true` will drop the table if it already exists
initDb = true;

assets = [];

User.sync({ force: initDb }).then(() => {
	//Do Nothing
  });
Asset.sync({ force: initDb }).then(() => {
	[1,2,3,4,5,6,7,8].forEach(ind => {
		Asset.create({
		assetName: 'token ' + ind,
		status: 'N'
		}).then(
			token => {
				console.log('token created with id=' + token.assetId)
				//console.log(assets);
				assets.push(token);
			}
		);
	});
  });
Loan.sync({ force: initDb }).then(() => {
	//Do Nothing
  });
updateCache = () => {
  Asset.findAll().then(results => {results.forEach(asset => {assetMap.put(asset.assetId, asset)})});
  User.findAll().then(results => {results.forEach(user => {userMap.put(user.usertId, user)})});
}
updateCache();
bot.use(async (ctx, next) => {
	  const start = new Date();
	  if (ctx.message != null){
		await User.findOne({
			where: {
			userId: ctx.message.from.id
			}
		}).then(result => {
			if (result == null){
				User.create({
					userName: ctx.message.from.first_name,
					userId: ctx.message.from.id
				}).then(user=>{ctx.reply('Welcome , ' + ctx.message.from.first_name + ', your account has been created');})
			} else {
				console.log('User already registered');
			}
		}).catch(error=>{console.error(error)});
		console.log('receving message %s from %s', ctx.message.text, ctx.message.from);  
		console.log(ctx.getChat());
	} else {
		console.log('receving message ', ctx);  
	}
	  await next();
	  const ms = new Date() - start;
	  console.log('Response time: %sms', ms)
});

getAssetMenu2 = () => { return Telegraf.Extra
  .markdown()
  .markup((m) => m.inlineKeyboard(
	assets.map(ele => {
		console.log(ele);
		return m.callbackButton('Loan ' + ele.assetName, ele.assetId);
	})
	).resize())
}


getAssetMenu = () => { return Telegraf.Extra.markup((markup) => (
	  assets.map(ele => {
		  console.log(ele);
		  return markup.callbackButton('Loan ' + ele.assetName, ele.assetId);
	  })
	))
};

bot.command('mine', (ctx)=>{
	(async() => {
		loans = await Loan.findAll({
			where:{
				endTime: null,
				userId: ctx.message.from.id
			}
		})
		menus = [];
		for(i=0;i<loans.length;i++){
			loan = loans[i]
			asset = await Asset.findOne({
				where: {
					assetId: loan.assetId
				}
			})
			menus.push(Telegraf.Markup.callbackButton('Release ' + loan.getAsset.assetName, 'release-' + loan.getAsset.assetId));
		
		}
		ctx.reply('Release', Telegraf.Markup.inlineKeyboard(menus).oneTime().resize().extra());
	})();
})

bot.command('menu', ({ reply }) => {
	return reply('Custom buttons keyboard', Telegraf.Markup
	  .keyboard([
		['🔍 Search', '😎 Popular'], // Row1 with 2 buttons
		['☸ Setting', '📞 Feedback'], // Row2 with 2 buttons
		['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
	  ])
	  .oneTime()
	  .resize()
	  .extra()
	)
});

bot.command('list', (ctx) => {
	menus = new Array();
	temp = new Array();
	//menus.push(temp);
	row = 0
	col = 0;
	Asset.findAll({
		where: {
			status : 'N'
		}
	}).then(result => {
		assets = result;
	})
	for(let i=0; i <assets.length; i++){
		if(temp.length == 3){
			menus.push(temp);
			temp = new Array();
			row ++;
		}
		temp.push(Telegraf.Markup.callbackButton(assets[i].assetName, 'loan-' + assets[i].assetId));
		col++;
	}
	if(temp.length > 0){
		menus.push(temp);
	}
	message_id = ctx.reply('Token to be used?',
	  Telegraf.Markup.inlineKeyboard(menus).oneTime().resize().extra()
	)
	console.info('message_id' + message_id)
	return message_id;
  })

const aboutMenu = Telegraf.Extra
  .markdown()
  .markup((m) => m.keyboard([
    m.callbackButton('⬅️ Back')
  ]).resize())

//bot.on('text', (ctx) => ctx.reply('Hello World'));
bot.command('list2', (ctx) => {
	ctx.reply('Click asset name to loan', getAssetMenu()).then(() => {
	  ctx.reply('about', aboutMenu)
	})
  })

bot.action(/^(loan-)(.*)$/, (ctx) => {
	//ctx.deleteMessage();
	ctx.editMessageText('Loaned!!');
	let assetId = ctx.match[2];
	Asset.findOne({
		where: {
			assetId: assetId
		}
	}).then(asset=>{
		if (asset.status == 'N'){
			asset.status = 'L'
			asset.save();
			Loan.create(
				{assetId: assetId, loanUserId: ctx.update.callback_query.from.id, startTime: new Date() }
			)
			User.findOne({
				where: {
					userId: ctx.update.callback_query.from.id
				}
			}).then(user=>{
				bot.telegram.sendMessage(chat_id, asset.assetName + ' was loaned by ' + user.userName)
			
				return ctx.answerCbQuery(asset.assetName + `loaned`);
			})
			
		} else {
			return ctx.reply('Resource alrady loan by other');
		}
		
	
	})

	return ctx.answerCbQuery(`Oh, ${ctx.match[2]}! Great choice`);
  })
  
  bot.catch((err, ctx) => {
	console.log(`Ooops, encountered an error for ${ctx.updateType}`, err)
  })
bot.command('create_user')
bot.launch();

