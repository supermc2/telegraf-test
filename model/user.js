const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class User extends Sequelize.Model { }
    User.init({
      userId: {type: DataTypes.INTEGER, primaryKey: true}, //telegram id
      userName: DataTypes.STRING
    }, { sequelize });
    User.associate = function (models) {
      models.User.belongsTo(models.Loan, {
        onDelete: "CASCADE",
        foreignKey: {
          allowNull: true
        }
      });
    };
    return User;
  }