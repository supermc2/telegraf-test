const Sequelize = require('sequelize');
const User = require('./user');
const Asset = require('./asset');
module.exports = (sequelize, DataTypes) => {
    class Loan extends Sequelize.Model { }
    Loan.init({
      loanId: {type: DataTypes.INTEGER , primaryKey: true, autoIncrement: true},
      //loanUserId: DataTypes.NUMBER,
      //assetId: DataTypes.NUMBER,
      remark: DataTypes.STRING,
      startTime: DataTypes.DATE,
      endTime: DataTypes.DATE
    }, { sequelize });
    Loan.associate = function(models) {
      models.Loan.hasOne(User);
      models.Loan.hasOne(Asset);
    };
    return Loan;
  }