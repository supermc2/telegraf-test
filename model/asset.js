const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Asset extends Sequelize.Model { }
    Asset.init({
        assetId: {type: DataTypes.INTEGER , primaryKey: true, autoIncrement: true},
        assetName: DataTypes.STRING,
        status: {type: DataTypes.STRING}
      
    }, { sequelize });
    Asset.associate = function (models) {
        models.Asset.belongsTo(models.Loan, {
          onDelete: "CASCADE",
          foreignKey: {
            allowNull: true
          }
        });
      };
    return Asset;
  }